<!DOCTYPE html>
<html>
<head><@lib.adminhead />

</head>
<body>

	<div id="wrapper">

		<#include "WEB-INF/templates/layout/inc/menu.ftl">
		<div id="page-wrapper">
			<div class="col-md-12 nopadding">
				<div id="tabs">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="home">
							<h1 class="page-header">欢迎来到系统后台</h1>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-laptop"></i> 系统操作
							</div>
							<div class="panel-body">
								<button type="button" class="btn btn-info btn-block" id="modify">修改密码</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<@lib.adminfootjs />

<script type="text/javascript" language="javascript">
	$(document).ready(function(e) {
		$('#tabs').addtabs({
			monitor : '#side-menu'
		});
	});
</script>
	<form method="post" action="${ctx}/admin/updatePwd" id="passwordForm" style="display: none">
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<label for="oldpassword">原密码</label> <input class="form-control" type="password" name="oldpassword" id="oldpassword"
							placeholder="原密码" required>
					</div>
					<div class="form-group">
						<label for="password">密码</label> <input class="form-control" type="password" name="password" id="password"
							placeholder="密码" required>
					</div>
					<div class="form-group">
						<label for="repassword">重复密码</label> <input class="form-control" type="password" name="repassword" id="repassword"
							placeholder="重复密码" required>
					</div>
					<div class="text-center">
						<input type="submit" class="btn btn-success" value="修改" />
					</div>
				</div>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		$(function() {
			$("#passwordForm").validate(
					{
						errorPlacement : function(error, element) {
							$(element).closest("form").find(
									"label[for='" + element.attr("id") + "']").append(error);
						},
						errorClass:'text-danger',
						errorElement: "span",
						rules : {
							oldpassword : {
								required : true,
								remote: "${ctx}/admin/checkUserPwd"
							},
							password : {
								required : true,
								minlength : 6,
								maxlength : 13
							},
							repassword : {
								equalTo : "#password"
							}
						},
						messages:{
							oldpassword : {
								remote: "原密码错误"
							},
							password : {
								minlength : '[密码6-13位]',
								maxlength : '[密码6-13位]'
							},
							repassword : {
								equalTo : '[两次输入密码不一致]'
							}
						},
						onkeyup : false
					});
			$("#modify").click(function() {
				var index = layer.open({
					type : 1,
					title : '修改密码',
					shadeClose : true, //点击遮罩关闭
					content : $("#passwordForm"),
					shade: 0
				});
			});
			
		});
	</script>
</body>
</html>
