<!DOCTYPE html>
<html>
<head>
<@lib.adminhead />
</head>
<body>
 <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">请登录..</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="${ctx}/admin/loginin">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="form-group">
									<div class="input-group">
										<input id="btn-input" type="text" name="randcode" class="form-control" placeholder="请输入验证码">
										<span class="input-group-btn">
											<img src="${ctx }/admin/captcha"  style="height:3rem" onclick="this.src='${ctx }/admin/captcha?'+Math.random()"/>
										</span>
									</div>
								</div>
                                <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<@lib.adminfootjs />
	</body>
</html>
