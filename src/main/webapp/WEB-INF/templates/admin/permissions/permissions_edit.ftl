<form name="form1" class="form-horizontal" method="post" action="" id="addform">
<input type="hidden" name="model.id" value="${model.id }" id="id" />
	<div class="form-group">
		<label for="type" class="col-sm-4 control-label">名称<span style="color: red">*</span></label>
		<div class="col-sm-5">
			 <input class="form-control" type="text"
				name="model.name" value="${model.name }" required />
		</div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-4 control-label">权限id<span style="color: red">*</span></label>
		<div class="col-sm-5">
			<input class="form-control" type="text" name="model.permission" id="permission" value="${ model.permission}" placeholder="示例 admin:admin:list"
				required />
		</div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-4 control-label">备注<span style="color: red">*</span></label>
		<div class="col-sm-5">
			<input class="form-control" type="text" name="model.remark" placeholder="描述信息" value="${model.remark }" required />
		</div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-4 control-label">父id<span style="color: red">*</span></label>
		<div class="col-sm-5">
			<input class="form-control" type="number" name="model.parent_id" id="parent_id" value="${model.parent_id}"  placeholder="0为根节点"
				required />
		</div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-4 control-label">排序<span style="color: red">*</span></label>
		<div class="col-sm-5">
			<input class="form-control" type="number" name="model.sort" value="${model.sort }" required />
		</div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-4 control-label">链接</label>
		<div class="col-sm-5">
			<input class="form-control" type="text" name="model.href" value="${model.href }" placeholder="示例 /admin/list" />
		</div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-4 control-label">是否为菜单<span style="color: red">*</span></label>
		<div class="col-sm-5">
			<label class="radio-inline"> 
				<input type="radio" name="model.is_show" value="0" checked="checked" />是
			</label> 
			<label class="radio-inline">
				<#if model.is_show==1>
				 <input type="radio" name="model.is_show" value="1" checked="checked" />否
				<#else>
				 <input type="radio" name="model.is_show" value="1" />否
				</#if>
			</label>
		</div>
	</div>
	<div class="text-center">
		<button class="btn btn-success" onclick="return oper_save(${model.id});">保 存</button>
	</div>
</form>

<script type="text/javascript">
$("#addform").validate(
		{
			errorClass:'text-danger',
			errorElement: "span",
			rules: {
				"model.permission": {
			      required: true,
			      remote: {
			        url: "${ctx}/admin/permissions/checkUnique",
			        type: "post",
			        data: {
			          id: function() {
			            return $( "#id" ).val();
			          }
			        }
			      }
			    }
			  },
			  messages: {
				    "model.permission": {
				    	remote: "权限id需唯一"
				    }
			}
		});
		function oper_save(id) {
			if($("#addform").valid()){
				id = id || '0'
				form1.action = "${ctx}/admin/permissions/save/"+id;
				form1.submit();
				return true;
			}
		}
	</script>
