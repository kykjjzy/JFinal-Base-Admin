<SCRIPT type="text/javascript">
	var setting = {
		check : {
			enable : true,
			chkStyle : "checkbox",
			chkboxType : {
				"Y" : "ps",
				"N" : "s"
			}
		},
		view : {
			showIcon : false
		},
		data : {
			simpleData : {
				enable : true
			}
		},
		callback : {
			onExpand : cookieSave,
			onCollapse : cookieRemove
		}
	};
	var IDMark_A = "_a";
	var zNodes = ${znodes};
	function showIconForTree(treeId, treeNode) {
		return !treeNode.isParent;
	};
	$(document).ready(function() {
		$.fn.zTree.init($("#treeDemo"), setting, zNodes);
		initExpand("treeDemo");
	});
	$("#saveperm").click(function(){
		var btn = $(this);
		btn.prop("disabled","disabled")
		btn.html("处理中..")
		var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
		var nodes = treeObj.getCheckedNodes(true);
		var str="";
		for(var i=0;i<nodes.length;i++){
			str += nodes[i].id;
			if(!(i===(nodes.length-1))){
				str += ",";
			}
		}
		$("#perms").val(str);
		$.getJSON("${ctx }/admin/roles/addperm", {"perms":$("#perms").val(),"roleid":$("#roleid").val()}, function(data){
				layer.msg(data.msg);
				btn.removeAttr("disabled")
				btn.html("保存")
			});
	})
</SCRIPT>
<div class="left">
	<ul id="treeDemo" class="ztree"></ul>
</div>
<br />
<form name="form1" class="form-horizontal" method="post" action="" id="addperm">
	<div class="text-center">
		<input type="hidden" name="perms" id="perms" >
		<input type="hidden" name="roleid" id="roleid" value="${roleid}">
		<button type="button" class="btn btn-success" id="saveperm">保存</button>
	</div>
</form>