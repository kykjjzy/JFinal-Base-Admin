<#-- 全局变量 -->
<#global path="${(ctp.contextPath)!''}">
<#global uploadpath="${ctx }/upload">
<#-- admin -->
<#macro adminhead>
<#include "WEB-INF/templates/layout/inc/header.ftl"/>
</#macro>
<#macro adminfootjs>
<#include "WEB-INF/templates/layout/inc/footjs.ftl" />
</#macro>
<#macro ztree>
<#include "WEB-INF/templates/layout/inc/ztree.ftl"/>
</#macro>
<#macro searchpage page>
<#include "WEB-INF/templates/layout/common/searchpage.ftl" /> <@searchpage page=page />
</#macro>
<#macro pickadate>
<script src="//cdn.bootcss.com/pickadate.js/3.5.6/compressed/picker.js"></script>
<script src="//cdn.bootcss.com/pickadate.js/3.5.6/compressed/picker.date.js"></script>
<script src="//cdn.bootcss.com/pickadate.js/3.5.6/compressed/translations/zh_CN.js"></script>
<link href="//cdn.bootcss.com/pickadate.js/3.5.6/compressed/themes/default.css" rel="stylesheet">
<link href="//cdn.bootcss.com/pickadate.js/3.5.6/compressed/themes/default.date.css" rel="stylesheet">
</#macro>
<#macro pupload>
	<script type="text/javascript" src="${ctx }/static/js/pupload/plupload.full.min.js"></script>
	<script src="//cdn.bootcss.com/plupload/2.3.1/i18n/zh_CN.js"></script>
</#macro>
<#macro mobilelayer>
<script src="//cdn.bootcss.com/layer/3.0/mobile/layer.min.js"></script>
<link href="//cdn.bootcss.com/layer/3.0/mobile/need/layer.css" rel="stylesheet">
</#macro>