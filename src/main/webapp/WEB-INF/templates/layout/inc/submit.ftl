<!DOCTYPE html>
<html>
<head><@lib.adminhead />
<body>
    <div id="wrapper">
		<#include "WEB-INF/templates/layout/inc/menu.ftl">
      <div id="page-wrapper" style="min-height: 448px;">
      <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">正在跳转..</h1>
                </div>
                <!-- /.col-lg-12 -->
         </div>
        <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
						<div class="panel-body">
							<div class="submitPage">
								<div class="alert alert-${waitPage.status}">
									<div>${waitPage.msg}</div>
									 <#if (waitPage.second>0) >
										<div>
											<strong id="second">${waitPage.second}</strong>
											秒后,如果您的浏览器没有自动跳转，请点 <a href="${ctx}${waitPage.url}"
												target="_self">这里</a>
										</div>
									</#if>
								</div>
							</div>
						</div>
					</div>
                  
                </div>
            </div>
        </div>
        </div>
        	<script type="text/javascript">
        	<#if (waitPage.second>0)>
	        var second =${waitPage.second};
	        setTimeout('showTime()',1000);
	        </#if>
	    function showTime()
	    {
	        if(second>=0)
	        {
	            document.getElementById('second').innerHTML = second+'';
	            second--;
	            setTimeout('showTime()',1000);
	        }
	        else
	        {
	            window.location.replace("${ctx}${waitPage.url}");
	        }
	    }
	</script>
<@lib.adminfootjs />
</body>

</html>
