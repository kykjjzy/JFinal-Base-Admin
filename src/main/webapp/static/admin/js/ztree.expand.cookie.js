/*!
 * 首先设置ztree,展开回调函数setting.callback.onExpand关闭回调函数setting.callback.onCollapse
 * var setting = {
 * 	callback: {
 * 		onExpand: cookieSave,
 * 		onCollapse: cookieRemove
 * 	}
 *};
 *
 *init 加入 initExpand()方法
 *
 */

function cookieSave(event, treeId, treeNode){
	if(!$.cookie("ztree_"+treeNode.tId)){
		$.cookie("ztree_"+treeNode.tId,1);
	}
}

function cookieRemove(event, treeId, treeNode) {
	$.removeCookie("ztree_"+treeNode.tId);
};

function initExpand(id){
	var treeObj = $.fn.zTree.getZTreeObj(id);
	var  datamap = $.cookie();
	for (key in datamap) {
		var node = treeObj.getNodeByTId(key);
		if (node!=null) {
			treeObj.expandNode(node);
		}
	}
}








