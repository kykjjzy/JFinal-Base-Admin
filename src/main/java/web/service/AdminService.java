package web.service;

import java.util.List;

import core.Const;
import web.model.AdminUser;
import web.model.Permissions;
import web.model.Roles;

public class AdminService {
	public static final AdminService me = new AdminService();
	private final AdminUser adminUserDao = new AdminUser().dao();

	public AdminUser getAdmin(long adminid) {
		return adminUserDao.findById(adminid);
	}
	public List<AdminUser> getList() {
		return adminUserDao.find("select * from admin_user");
	}

	public AdminUser getUserByName(String username) {
		return adminUserDao.findFirst("select * from admin_user where username  = ?", username);
	}

	public List<Roles> getRoles(Long adminid) {
		List<Roles> roles = Roles.dao
				.findByCache(Const.ADMIN_ROLE_CACHE, Const.ADMIN_ROLE_CACHE + "-" + adminid,
						"select * from roles where id in (SELECT role_id FROM admin_role WHERE admin_id=?)", adminid);
		return roles;
	}

	public List<Permissions> getPermission(Long adminid) {
		List<Permissions> perms = Permissions.dao.findByCache(Const.ADMIN_PERM_CACHE,
				Const.ADMIN_PERM_CACHE + "-" + adminid,
				"select * from permissions where id in (SELECT perm_id FROM admin_perm WHERE admin_id=?)", adminid);
		return perms;
	}
}
