package web.common;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.core.Controller;

import web.model.temp.WaitPage;

public class BaseController extends Controller {
	private static boolean ISTRUE = false;

	private static String TIP = "操作失败";
	public void sendWaitPage(String msg, String url, String status, int second) {
		WaitPage waitPage = new WaitPage();
		waitPage.setMsg(msg);
		waitPage.setSecond(second);
		waitPage.setUrl(url);
		waitPage.setStatus(status);
		setAttr("waitPage", waitPage);
		render("/WEB-INF/templates/layout/inc/submit.ftl");
	}

	public void sendSuccessWaitPage(String msg, String url) {
		sendWaitPage(msg, url, "success", 3);

	}

	public void sendErrorWaitPage(String msg, String url) {
		sendWaitPage(msg, url, "danger", 3);
	}

	public void sendAlert(String msg, String url) {
		try {
			if (StringUtils.isBlank(msg)) {
				redirect(url);
				return;
			} else {
				if (StringUtils.contains(url, "?")) {
					redirect(url + "&globalmessage=" + URLEncoder.encode(msg, "UTF-8"));
				} else {
					redirect(url + "?globalmessage=" + URLEncoder.encode(msg, "UTF-8"));
				}
				return;
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}


}
