package kit;

import com.jfinal.plugin.ehcache.CacheKit;

import core.Const;

public class EhCacheKit extends CacheKit {
	public static void cleanRoleAndPerm() {
		removeAll(Const.ADMIN_ROLE_CACHE);
		removeAll(Const.ADMIN_PERM_CACHE);
	}
}
